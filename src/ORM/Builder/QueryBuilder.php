<?php

namespace Orchestra\ORM\Builder;

use Orchestra\ORM\Builder\Traits\ReadTrait;
use Orchestra\ORM\Connector;

class QueryBuilder
{
   use ReadTrait;

   protected $table;

   protected $fields;

   protected $instanceClass;

   public function __construct($instanceClass)
   {
      $this->instanceClass = $instanceClass;
   }
}
