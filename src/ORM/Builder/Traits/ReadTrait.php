<?php

namespace Orchestra\ORM\Builder\Traits;

use Orchestra\Helpers\Arr\Arr;
use Orchestra\Helpers\Collection\Collection;
use Orchestra\ORM\Connector;
use Orchestra\ORM\Exceptions\InvalidOrientationException;

trait ReadTrait
{
   protected $query;

   protected $whereFields = [];

   protected $orderFields = [];

   protected $groupFields = [];

   public function select($fields = "*")
   {
      $this->fields = $fields;

      return $this;
   }

   public function from($table)
   {
      $this->table = $table;

      return $this;
   }

   public function where($field, $operator = null, $value = null)
   {
      $this->whereFields[] = [
         "field"    => $field,
         "operator" => is_null($value) ? "=" : $operator,
         "value"    => is_null($value) ? $operator : $value
      ];

      return $this;
   }

   public function orderBy($field, $orientation = "ASC")
   {
      $this->orderFields[] = [
         "field"       => $field,
         "orientation" => strtoupper($orientation)
      ];

      return $this;
   }

   public function groupBy($field, $orientation = "ASC")
   {
      $this->groupFields[] = [
         "field"       => $field,
         "orientation" => strtoupper($orientation)
      ];

      return $this;
   }

   public function getSql()
   {
      return $this->generateSelectQuery();
   }

   public function get()
   {
      $connector = new Connector();

      $connection = $connector->connection;

      $this->query = $this->generateSelectQuery();

      $statment = $connection->prepare($this->query);

      $parameters = $this->generateQueryParameters();

      $statment->execute($parameters);

      $data = $statment->fetchAll(\PDO::FETCH_ASSOC);

      $connector->close();

      return Collection::from($data)->transform(function ($item) {

         $instanceClass = $this->instanceClass;

         $model = new $instanceClass($item);

         return $model;
      });
   }

   protected function generateSelectQuery()
   {
      $query = "SELECT $this->fields FROM $this->table";

      $whereFields = $this->getPreparedWhereFields();

      if (!empty($whereFields)) {
         $query .= " WHERE $whereFields";
      }

      $groupFields = $this->getPreparedGroupByFields();

      if (!empty($groupFields)) {
         $query .= " GROUP BY $groupFields";
      }

      $orderFields = $this->getPreparedOrderByFields();

      if (!empty($orderFields)) {
         $query .= " ORDER BY $orderFields";
      }

      return $query;
   }

   protected function generateQueryParameters()
   {
      $whereFields = Collection::from($this->whereFields)
         ->mapWithKeys(function ($item) {

            $field = $item["field"];

            $value = $item["value"];

            return [":$field" => $value];
         });

      return $whereFields;
   }

   protected function getPreparedWhereFields()
   {
      return Collection::from($this->whereFields)
         ->transform(function ($item) {

            $field = $item["field"];

            $operator = $item["operator"];

            return "$field $operator :$field";
         })->join(" AND ");
   }

   protected function getPreparedOrderByFields()
   {
      return Collection::from($this->orderFields)
         ->transform(function ($item) {
            $field = Arr::get($item, "field");

            $orientation = strtoupper($item["orientation"]);

            if ($orientation != "ASC" && $orientation != "DESC") {
               throw new InvalidOrientationException("$orientation is not a valid SQL Order By orientation");
            }

            return "$field $orientation";
         })->join(", ");
   }

   protected function getPreparedGroupByFields()
   {
      return Collection::from($this->groupFields)
         ->transform(function ($item) {
            $field = Arr::get($item, "field");

            $orientation = strtoupper($item["orientation"]);

            if ($orientation != "ASC" && $orientation != "DESC") {
               throw new InvalidOrientationException("$orientation is not a valid SQL Order By orientation");
            }

            return "$field $orientation";
         })->join(", ");
   }
}
