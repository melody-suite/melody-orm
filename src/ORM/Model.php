<?php

namespace Orchestra\ORM;

use JsonSerializable;
use Orchestra\Helpers\Arr\Arr;
use Orchestra\Helpers\Collection\Collection;
use Orchestra\ORM\Builder\QueryBuilder;
use Orchestra\ORM\Exceptions\FieldNotMappedException;

abstract class Model implements JsonSerializable
{
   protected $fields;
   protected $attributes = [];
   protected $queryBuilder;

   public abstract function fields(): array;

   public abstract function table(): string;

   public abstract function primaryKey(): string;

   public function __construct($attributes = [])
   {
      $this->fields = $this->fields();

      $this->queryBuilder = new QueryBuilder(get_class($this));

      $this->fill($attributes);
   }

   public static function query()
   {
      $instance = new static();

      return $instance->queryBuilder->from($instance->table());
   }

   public static function selectRaw($select)
   {
      $instance = new static();

      return $instance->queryBuilder->select($select)
         ->from($instance->table());
   }

   public static function find($id)
   {
      $instance = new static();

      return $instance->queryBuilder->select()
         ->from($instance->table())
         ->where($instance->primaryKey(), $id)
         ->get()
         ->first();
   }

   public static function where($field, $operator = null, $value = null)
   {
      $instance = new static();

      return $instance->queryBuilder->select()
         ->from($instance->table())
         ->where($field, $operator, $value);
   }

   public static function all()
   {
      $instance = new static();

      return $instance->queryBuilder->select()
         ->from($instance->table())
         ->get();
   }

   public function fill(array $attributes)
   {
      Collection::from($attributes)
         ->each(function ($item, $key) {
            $this->{$key} = $item;
         });

      return $this;
   }

   public function __get($key)
   {
      if (!$this->fieldIsMapped($key)) {
         throw new FieldNotMappedException($this->getFieldNotMappedErrorMessage($key));
      }

      return Arr::get($this->attributes, $key);
   }

   public function __set($key, $value)
   {
      if (!$this->fieldIsMapped($key)) {
         throw new FieldNotMappedException($this->getFieldNotMappedErrorMessage($key));
      }

      Arr::set($this->attributes, $key, $value);
   }

   public function toArray()
   {
      return $this->attributes;
   }

   public function jsonSerialize()
   {
      return $this->toArray();
   }

   public function __toString()
   {
      return Arr::join($this->toArray(), ",");
   }

   private function fieldIsMapped($key)
   {
      return Arr::exists($this->fields, $key);
   }

   private function getFieldNotMappedErrorMessage($key)
   {
      return "$key not mapped, please map $key field in fields() method.";
   }
}
