<?php

namespace Orchestra\ORM\Exceptions;

class FieldNotMappedException extends \Exception
{
}
