<?php

namespace Orchestra\ORM;

class Connector
{
   private $driver;

   private $host;

   private $database;

   private $user;

   private $password;

   private $connection;

   public function __construct()
   {
      $this->driver = env("DATABASE_DRIVER");

      $this->host = env("DATABASE_HOST");

      $this->database = env("DATABASE_NAME");

      $this->user = env("DATABASE_USER");

      $this->password = env("DATABASE_PASSWORD");

      $this->open();
   }

   public function __get($name)
   {
      return $this->{$name};
   }

   public function __set($name, $value)
   {
      return $this->{$name} = $value;
   }

   public function open()
   {
      if (!empty($this->connection)) {
         return $this->connection;
      }

      $this->connection = new \PDO("$this->driver:host=$this->host;dbname=$this->database", $this->user, $this->password);
   }

   public function close()
   {
      if (empty($this->connection)) {
         return;
      }

      $this->connection = null;
   }
}
